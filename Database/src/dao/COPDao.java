package dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConFact;
import model.Customer;
import model.Order;
import model.Product;


/**
 * Clasa care contine metodele pentru a interactiona asupra bazei de date
 * @author Tam�s
 *
 */
public class COPDao {

	protected static final Logger LOGGER = Logger.getLogger(COPDao.class.getName());
	private static final String insertStatementString1 = "INSERT INTO Customer (name,age,email,id,address)"
			+ " VALUES (?,?,?,?,?)";
	private static final String updateStatementString1 = "UPDATE Customer SET name=?,age=?,email=?,id=?,address=? where id=?";
			
	private static final String deleteStatementString1 = "DELETE FROM Customer where id =?";
	private final static String findStatementString1 = "SELECT * FROM Customer where id = ?";
	private final static String findStatementString11 = "SELECT * FROM Product where name = ?";
	public static final String jTString1= "SELECT * FROM Customer";
	public static final String jTString2= "SELECT * FROM Product";
	private static final String insertStatementString3 = "INSERT INTO Product (id,name,cantity,price,quality)"
			+ " VALUES (?,?,?,?,?)";
	private static final String updateStatementString3 = "UPDATE Product SET id=?,name=?,cantity=?,price=?,quality=? where id=?";
	private static final String deleteStatementString3 = "DELETE  FROM Product where id=?";
	private final static String findStatementString3 = "SELECT * FROM Product where id = ?";
	private static final String ordering = "INSERT INTO tpdb.Order (orderBy,productName,address,cantity)"
	         + " VALUES(?,?,?,?)";
	private static final String stock= "UPDATE Product SET cantity=? where name=?";		
	
	
	/**
	 * Metoda gaseste un Customer in tabela customer dupa id-ul specificat
	 * @param customerId
	 * @return
	 */
	
	public static Customer findCustById(int customerId) {
		Customer toReturn = null;

		Connection dbConnection = ConFact.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString1);
			findStatement.setLong(1, customerId);
			rs = findStatement.executeQuery();
			rs.next();

			String name = rs.getString("name");
			String address = rs.getString("address");
			String email = rs.getString("email");
			int age = rs.getInt("age");
			toReturn = new Customer(name,age,email,customerId,address);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"COPDao:findcustById " + e.getMessage());
		} finally {
			ConFact.close(rs);
			ConFact.close(findStatement);
			ConFact.close(dbConnection);
		}
		return toReturn;
	}
	
	
	/**
	 * Metoda gaseste un Product in tabela product dupa id-ul specificat
	 * @param prodId
	 * @return
	 */
	
	public static Product findProdById(int prodId) {
		Product toReturn = null;

		Connection dbConnection = ConFact.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString3);
			findStatement.setLong(1, prodId);
			rs = findStatement.executeQuery();
			rs.next();

			String name = rs.getString("name");
			int cantity =rs.getInt("cantity");
			int price =rs.getInt("price");
			int quality=rs.getInt("quality");
			toReturn = new Product(prodId,name,cantity,price,quality);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"COPDao:findprodById " + e.getMessage());
		} finally {
			ConFact.close(rs);
			ConFact.close(findStatement);
			ConFact.close(dbConnection);
		}
		return toReturn;
	}
	/**
	 * Metoda gaseste un Product in tabela product dupa numelui specificat
	 * @param Name
	 * @return toReturn Produsul cautat
	 */
	
	public static Product findProdByName(String Name) {
		Product toReturn = null;

		Connection dbConnection = ConFact.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString11);
			findStatement.setString(1, Name);
			rs = findStatement.executeQuery();
			rs.next();

			int prodId = rs.getInt("id");
			int cantity =rs.getInt("cantity");
			int price =rs.getInt("price");
			int quality=rs.getInt("quality");
			toReturn = new Product(prodId,Name,cantity,price,quality);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"COPDao:findprodByName " + e.getMessage());
		} finally {
			ConFact.close(rs);
			ConFact.close(findStatement);
			ConFact.close(dbConnection);
		}
		return toReturn;
	}
	
	
	/**
	 * Metoda insereaza in customer in baza de date
	 * @param cust
	 * @return
	 */
	
	
	public static int insertCust(Customer cust) {
		Connection dbConnection = ConFact.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString1, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, cust.getName());
			insertStatement.setInt(2, cust.getAge());
			insertStatement.setString(3, cust.getEmail());
			insertStatement.setInt(4, cust.getID());
			insertStatement.setString(5, cust.getAddress());
			
			
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "COPDao:insert " + e.getMessage());
		} finally {
			ConFact.close(insertStatement);
			ConFact.close(dbConnection);
		}
		return insertedId;
	}
	
	/**
	 * Metoda insereaza un produs in baza de date
	 * @param prod
	 * @return
	 */
	
	public static int insertProd(Product prod) {
		Connection dbConnection = ConFact.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString3, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, prod.getID());
			insertStatement.setString(2, prod.getName());
			insertStatement.setInt(3, prod.getCantity());
			insertStatement.setInt(4, prod.getPrice());
			insertStatement.setInt(5, prod.getQuality());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "COPDao:insert " + e.getMessage());
		} finally {
			ConFact.close(insertStatement);
			ConFact.close(dbConnection);
		}
		return insertedId;
	}
	
	
	/**
	 * Metoda insereaza in tabele Order a bazei de date
	 * @param o
	 * @return
	 */
	
	
	public static int insertOrder(Order o) {
		Connection dbConnection = ConFact.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(ordering, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, o.getOrderBy());
			insertStatement.setString(2, o.getProductName());
			insertStatement.setString(3, o.getAddress());
			insertStatement.setInt(4, o.getCantity());
			
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
				
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "COPDao:insert " + e.getMessage());
		} finally {
			ConFact.close(insertStatement);
			ConFact.close(dbConnection);
		}
		return insertedId;
	}
	
	
	
	
	
	/**
	 * Metoda sterge un client din tabela Customer a bazei de date
	 * @param id
	 * @return
	 */
	
	
	
	public static int deleteCust(int id) {
		Connection dbConnection = ConFact.getConnection();

		PreparedStatement deleteStatement = null;
		int deleteId = -1;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString1, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, id);
			
			
			deleteStatement.executeUpdate();

			ResultSet rs = deleteStatement.getGeneratedKeys();
			if (rs.next()) {
				deleteId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "COPDao:insert " + e.getMessage());
		} finally {
			ConFact.close(deleteStatement);
			ConFact.close(dbConnection);
		}
		return deleteId;
	}
	
	/**
	 * Metoda sterge un produs din tabela Product a bazei de date
	 * @param prod
	 * @return
	 */
	
	
	
	public static int deleteProd(int prod) {
		Connection dbConnection = ConFact.getConnection();

		PreparedStatement deleteStatement = null;
		int deleteId = -1;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString3, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, prod);
			deleteStatement.executeUpdate();

			ResultSet rs = deleteStatement.getGeneratedKeys();
			if (rs.next()) {
				deleteId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "COPDao:delete " + e.getMessage());
		} finally {
			ConFact.close(deleteStatement);
			ConFact.close(dbConnection);
		}
		return deleteId;
	}
	
	/**
	 * Metoda actualizeaza un client in tabela Customer a bazei de date
	 * @param cust
	 * @return
	 */
	
	public static int updateCust(Customer cust) {
		Connection dbConnection = ConFact.getConnection();

		PreparedStatement updateStatement = null;
		int insertedId = -1;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString1, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setString(1, cust.getName());
			updateStatement.setInt(2, cust.getAge());
			updateStatement.setString(3, cust.getEmail());
			updateStatement.setInt(4, cust.getID());
			updateStatement.setString(5, cust.getAddress());
			updateStatement.setInt(6, cust.getID());
			
			updateStatement.executeUpdate();

			ResultSet rs = updateStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "COPDao:update " + e.getMessage());
		} finally {
			ConFact.close(updateStatement);
			ConFact.close(dbConnection);
		}
		return insertedId;
	}
	
	/**
	 * Metoda actualizeaza un produs in tabela Product a bazei de date
	 * @param prod
	 * @return
	 */
	
	
	public static int updateProd(Product prod) {
		Connection dbConnection = ConFact.getConnection();

		PreparedStatement updateStatement = null;
		int insertedId = -1;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString3, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setInt(1, prod.getID());
			updateStatement.setString(2, prod.getName());
			updateStatement.setInt(3, prod.getCantity());
			updateStatement.setInt(4, prod.getPrice());
			updateStatement.setInt(5, prod.getQuality());
			updateStatement.setInt(6, prod.getID());
			
			updateStatement.executeUpdate();

			ResultSet rs = updateStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "COPDao:update " + e.getMessage());
		} finally {
			ConFact.close(updateStatement);
			ConFact.close(dbConnection);
		}
		return insertedId;
	}
	
	
	/**
	 * Metoda actualizeaza un produs in tabela Product a bazei de date
	 * @param quant Cantitatea care trebuie se scade din cantitatea originala
	 * @param prod Produs specificat
	 * @return
	 */
	
	public static int updateProdC(int quant,Product prod) {
		Connection dbConnection = ConFact.getConnection();

		PreparedStatement updateStatement = null;
		int insertedId = -1;
		if (quant<prod.getCantity()){
		try {
			insertedId=1;
			updateStatement = dbConnection.prepareStatement(stock, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setInt(1, prod.getCantity()-quant);
			updateStatement.setString(2, prod.getName());
			
			updateStatement.executeUpdate();

			ResultSet rs = updateStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "COPDao:update " + e.getMessage());
		} finally {
			
			ConFact.close(updateStatement);
			ConFact.close(dbConnection);
			
		}
		return insertedId;
		}
		else {System.out.println("There are less available product");
		return -1;
		}
	
		}
	
	
	
	
	
	/*public static ResultSet jTpop() {

		Connection dbConnection = ConFact.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		int insertedId;
		try {
			findStatement = dbConnection.prepareStatement(jTString1);
			rs = findStatement.executeQuery();
			rs.next();
			
		
			
			

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"COPDao:populate " + e.getMessage());
		} finally {
			ConFact.close(rs);
			ConFact.close(findStatement);
			ConFact.close(dbConnection);
		}
		return rs;
	}*/
	
	
	
	
	
	
	
	
	
	
	
	
	
}
