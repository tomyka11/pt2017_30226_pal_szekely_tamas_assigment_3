package model;

/**
 * Clasa pentru a modele daca un client comanda ceva
 * @author Tam�s
 *
 */
public class Order {
	private String orderBy;
	private String productName;
	private String address;
	private int cantity;
	
	public Order(){
		super();
	}
	/**
	 * Contructorul clasei
	 * @param orderBy Numele cine comanda
	 * @param productName Numele produsului comandat
	 * @param address Adresa lui cine a comandat
	 * @param cantity Cantitatea produsului
	 */
	public Order(String orderBy,String productName,String address,int cantity){
		super();
		this.orderBy=orderBy;
		this.productName=productName;
		this.address=address;
		this.cantity=cantity;
	}
	
	/**
	 *Metoda returneaza numele clientului cine comanda
	 * @return
	 */
	public String getOrderBy(){
		return this.orderBy;
	}
	/**
	 * Metoda seteaza numele clientului
	 * @param orderBy
	 */
	
	public void setOrderBy(String orderBy){
		this.orderBy=orderBy;
	}
	/**
	 * Metoda returneaza numele produsului comandat
	 * @return
	 */
	
	public String getProductName(){
		return this.productName;
	}
	/**
	 * Metoda seteaza numele produsului
	 * @param productName
	 */
	
	public void setProductName(String productName){
		this.productName=productName;
	}
	/**
	 * Metoda returneaza adresa
	 * @return
	 */
	
	public String getAddress(){
		return this.address;
	}
	
	/**
	 * Metoda seteaza adresa
	 * @param address
	 */
	public void setAddress(String address){
        this.address=address;
	}
	/**
	 * Metoda returneaza cantitatea comandata
	 * @return
	 */
	
	public int getCantity(){
		return this.cantity;
	}
	
	/**
	 * Metoda seteaza cantitatea comandata
	 * @param cantity
	 */
	public void setCantity(int cantity){
		this.cantity=cantity;
	}
	
	public String toString(){
		return  this.cantity+" "+this.productName+"was ordered by: "+this.orderBy+"to address: "+this.address;
	
	}

}
