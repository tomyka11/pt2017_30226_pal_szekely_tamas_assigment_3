package model;

/**
 * Clasa prin care modelam un anumit client 
 * @author Tam�s
 *
 */

public class Customer {
	private String name;
	private int age;
	private String email;
	private int id;
	private String address;
	public Customer(){
		super();
	}
	/**
	 * Constructorul clasei
	 * @param name Numele clientului
	 * @param age Varsta clientului
	 * @param email Emailul clientului
	 * @param id Id-ul clientului
	 * @param address Adresa clientului
	 */
	
	public Customer(String name,int age,String email,int id,String address){
		super();
		this.name=name;
		this.age=age;
		this.email=email;
		this.id=id;
		this.address=address;
	}
	/**
	 * Metoda returneaza numele clientului
	 * @return
	 */
	public String getName(){
		return this.name;
	}
	/**
	 * Metoda seteaza numele clientului
	 * @param name
	 */
	public void setName(String name){
		this.name=name;
	}
	/**
	 * Metoda returneaza varsta clientului
	 * @return
	 */
	
	public int getAge(){
		return this.age;
	}
	
	/**
	 * Metoda seteaza varsta clientului
	 * @param age
	 */
	public void setAge(int age){
		this.age=age;
	}
	
	/**
	 * Metoda returneaza emailul clientului
	 * @return
	 */
	public String getEmail(){
		return this.email;
	}
	/**
	 * Metoda seteaza emailul clientului
	 * @param email
	 */
	
	public void setEmail(String email){
		this.email=email;
	}
	/**
	 * Metoda returneaza Id-ul clientului
	 * @return
	 */
	
	public int getID(){
		return this.id;
	}
	/**
	 * Metoda seteaza idul clientului daca e nevoie
	 * @param id
	 */
	
	public void setID(int id){
		this.id=id;
	}
	
	/**
	 * Metoda returneaza adresa clientului
	 * @return
	 */
	public String getAddress(){
		return this.address;
	}
	/**
	 * Metoda seteaza adresa clientului
	 * @param address
	 */
	
	public void setAddress(String address){
		this.address=address;
	}
	
	public String toString() {
		return "Customer [id=" + this.id + ", name=" + this.name + ", address=" + this.address + ", email=" + this.email + ", age=" + this.age
				+ "]";
	}

}
