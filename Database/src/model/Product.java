package model;

/**
 * Clasa prin care modelam produsul
 * @author Tam�s
 *
 */
public class Product {
  private int id;
  private String name;
  private int cantity;
  private int price;
  private int quality;
  
  public Product(){
	  super();
  }
  
  /**
   * Constructorul clasei produs
   * @param id Idul produsului
   * @param name Numele produsului
   * @param cantity Cantitatea produsului
   * @param price Pretul produsului
   * @param quality Calitatea produsului
   */
  
  public Product(int id,String name,int cantity,int price,int quality){
	  super();
	  this.id=id;
	  this.name=name;
	  this.cantity=cantity;
	  this.price=price;
	  this.quality=quality;
	  
  }
  /**
   * Metoda returneaza idul produsului
   * @return
   */
  
  public int getID(){
	  return this.id;
  }
  /**
   * Metoda seteaza idul produsului
   * @param id
   */
  
  public void setID(int id){
	  this.id=id;
  }
  /**
   * Metoda returneaza numele produsului
   * @return
   */
  
  public String getName(){
	  return this.name;
  }
  /**
   * Metoda seteaza numele produsului
   * @param name
   */
  
  public void setName(String name){
	  this.name=name;
  }
  /**
   * Metoda returrneaza cantitatea produsului
   * @return
   */
  
  public int getCantity(){
	  return this.cantity;
  }
  /**
   * Metoda seteaza cantitatea produsului
   * @param cantity
   */
	
  public void setCantity(int cantity){
	  this.cantity=cantity;
  }
  /**
   * Metoda returneaza pretul produsului
   * @return
   */
  
  public int getPrice(){
	  return this.price;
  }
  
  /**
   * Metoda seteaza pretul produsului
   * @param price
   */
  public void setPrice(int price){
	  this.price=price;
  }
  /**
   * Metoda returneaza calitatea produsului
   * @return
   */
  
  public int getQuality(){
	  return this.quality;
  }
  /**
   * Metoda seteaza calitatea produsului
   * @param quality
   */
  
  public void setQuality(int quality){
	  this.quality=quality;
	  
  }
  
  public String toString() {
		return "Product [id=" + this.id + ", name=" + this.name + ", cantity=" + this.cantity + ", price=" + this.price + ", quality=" + this.quality
				+ "]";
	}
  
}
