package blll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.EmailValidator;
import bll.validators.CustomerAgeValidator;
import bll.validators.Validator;
import dao.COPDao;
import model.Customer;
import model.Product;

/**
 * Clasa prin care se facem interactiunile asupra bazi de date -Product
 * @author Tam�s
 *
 */
public class ProductBLL {
	
	private List<Validator<Product>> validators;
	
	public ProductBLL() {
		validators = new ArrayList<Validator<Product>>();
	}
	/**
	 * Metoda cauta dupa un produs in tabela Product a bazei de date
	 * @param id
	 * @return
	 */
	
	public Product findProductById(int id) {
		Product st = COPDao.findProdById(id);
		if (st == null) {
			throw new NoSuchElementException("The product with id =" + id + " was not found!");
		}
		return st;
	}
	/**
	 * Metoda insereaza un produs la tabela Product a bazei de date
	 * @param p
	 * @return
	 */
	
	public int insertProduct(Product p) {
		for (Validator<Product> v : validators) {
			v.validate(p);
		}
		return COPDao.insertProd(p);
	}
	/**
	 * Metoda sterge un produs din tabela Product a bazei de date
	 * @param p 
	 * @return
	 */

	public int deleteProduct(int p) {
		return COPDao.deleteProd(p);
	}
	/**
	 * Metoda actualizeaza un produs dupa ID-ul produsului
	 * @param product
	 * @return
	 */
	
	public int updateProduct(Product product) {
		for (Validator<Product> v : validators) {
			v.validate(product);
		}
		return COPDao.updateProd(product);
	}
	
	/**
	 * Metoda actualizeaza cantitatea de produs dat ca si parametru
	 * @param quant
	 * @param prod
	 * @return
	 */
	public int updateProdtoOrd(int quant,Product prod){
		return COPDao.updateProdC(quant, prod);
	}
	
	
}