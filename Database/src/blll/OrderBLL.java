package blll;

import java.util.ArrayList;

import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.EmailValidator;
import bll.validators.CustomerAgeValidator;
import bll.validators.Validator;
import dao.COPDao;
import model.Customer;
import model.Product;
import model.Order;

/**
 * Clasa prin care se facem interactiunile asupra bazei de date -Order
 * @author Tam�s
 *
 */

public class OrderBLL {
	
	private List<Validator<Order>> validators;
	
	public OrderBLL() {
		validators = new ArrayList<Validator<Order>>();
	}
	
	/*public Product findOrderById(int id) {
		Product st = COPDao.findOrdById(id);
		if (st == null) {
			throw new NoSuchElementException("The product with id =" + id + " was not found!");
		}
		return st;
	}
	*/
	/**
	 * Metoda insereaza in tabela Order in baza de date
	 * @param o
	 * @return 
	 */
	
	
	public int insertOrder(Order o) {
		//for (Validator<Order> v : validators) {
			//v.validate(o);
		//}
		return COPDao.insertOrder(o);
	}
	
	

	
	
	
}