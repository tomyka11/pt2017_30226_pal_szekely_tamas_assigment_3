package blll;

import java.util.ArrayList;

import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.EmailValidator;
import bll.validators.CustomerAgeValidator;
import bll.validators.Validator;
import dao.COPDao;
import model.Customer;
/**
 * Clasa prin care se facem interactiunile asupra bazei de date - Customer
 * @author Tam�s
 *
 */

public class CustomerBLL {
	
	private List<Validator<Customer>> validators;
	/**
	 * Constructorul clasei se creaza doua obiecte de tip EmailValidator si CustomerAgeValidator
	 */
	
	public CustomerBLL() {
		validators = new ArrayList<Validator<Customer>>();
		validators.add(new EmailValidator());
		validators.add(new CustomerAgeValidator());
	}
	
	/**
	 * Metoda se cauta in baza de data dupa un Customer dupa id-ul lui
	 * @param id
	 * @return
	 */
	public Customer findCustomerById(int id) {
		Customer st = COPDao.findCustById(id);
		if (st == null) {
			throw new NoSuchElementException("The customer with id =" + id + " was not found!");
		}
		return st;
	}
	/**
	 * Metoda insereaza un Customer in baza de date
	 * @param customer
	 * @return
	 */
	
	public int insertCustomer(Customer customer) {
		for (Validator<Customer> v : validators) {
			v.validate(customer);
		}
		return COPDao.insertCust(customer);
	}
	/**
	 * Metoda sterge un Customer din baza de date
	 * @param id
	 * @return
	 */
	
	public int deleteCustomer(int id) {
		return COPDao.deleteCust(id);
	}
	/**
	 * Metoda actualizeaza un anumit Customer in baza de date
	 * @param customer
	 * @return
	 */
	
	public int updateCustomer(Customer customer) {
		for (Validator<Customer> v : validators) {
			v.validate(customer);
		}
		return COPDao.updateCust(customer);
	}

}
