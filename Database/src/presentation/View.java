package presentation;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class View extends JFrame {
	
	private JTextField cust_name=new JTextField(15);
	private JTextField cust_age=new JTextField(15);
	private JTextField cust_email=new JTextField(15);
	private JTextField cust_id=new JTextField(15);
	private JTextField cust_address=new JTextField(15);
	private JTextField prod_id=new JTextField(15);
	private JTextField prod_name=new JTextField(15);
	private JTextField prod_cantity=new JTextField(15);
	private JTextField prod_price=new JTextField(15);
	private JTextField prod_quality=new JTextField(15);
	private JTextField order_ob=new JTextField(15);
	private JTextField order_pn=new JTextField(15);
	private JTextField order_a=new JTextField(15);
	private JTextField order_c=new JTextField(15);
	private JButton add_cust=new JButton("ADD");
	private JButton edit_cust=new JButton("EDIT");
	private JButton delete_cust=new JButton("DELETE");
	private JButton find_cust=new JButton("FIND");
	private JButton add_prod=new JButton("ADD");
	private JButton edit_prod=new JButton("EDIT");
	private JButton delete_prod=new JButton("DELETE");
	private JButton find_prod=new JButton("FIND");
	private JButton ord=new JButton("ORDER");
	
	
	public View(){
		
		
		
		JPanel p=new JPanel();
		p.setLayout(new BoxLayout(p,BoxLayout.Y_AXIS));
		JPanel pan1=new JPanel();
		pan1.setLayout(new FlowLayout());
		JPanel pan2=new JPanel();
		pan2.setLayout(new BoxLayout(pan2,BoxLayout.X_AXIS));
		JPanel pan3=new JPanel();
		pan3.setLayout(new FlowLayout());
		JPanel pan4=new JPanel();
		pan4.setLayout(new FlowLayout());
		JPanel pan5=new JPanel();
		pan5.setLayout(new FlowLayout());
		JPanel pan6=new JPanel();
		pan6.setLayout(new FlowLayout());
		JPanel pan7=new JPanel();
		pan7.setLayout(new BoxLayout(pan7,BoxLayout.X_AXIS));
		JPanel pan8=new JPanel();
		pan8.setLayout(new FlowLayout());
		JPanel pan9=new JPanel();
		pan9.setLayout(new FlowLayout());
		JPanel pan10=new JPanel();
		pan10.setLayout(new FlowLayout());
		JPanel pan11=new JPanel();
		pan11.setLayout(new FlowLayout());
		JPanel pan12=new JPanel();
		pan12.setLayout(new BoxLayout(pan12,BoxLayout.X_AXIS));
		JPanel pan13=new JPanel();
		pan13.setLayout(new FlowLayout());
		JPanel pan14=new JPanel();
		pan14.setLayout(new FlowLayout());
		
		
		p.add(pan1);
		p.add(pan2);
		p.add(pan3);
		p.add(pan4);
		//p.add(pan5);
		p.add(pan6);
		p.add(pan7);
		p.add(pan8);
		p.add(pan9);
		//p.add(pan10);
		p.add(pan11);
		p.add(pan12);
		p.add(pan13);
		p.add(pan14);
		
		
		pan1.add(new JLabel("Customer Operations"));
		pan1.setBackground(Color.GREEN);
		pan2.add(new JLabel("Name"));
		pan2.add(Box.createRigidArea(new Dimension(150,0)));
		pan2.add(new JLabel("Age"));
		pan2.add(Box.createRigidArea(new Dimension(160,0)));
		pan2.add(new JLabel("Email"));
		pan2.add(Box.createRigidArea(new Dimension(150,0)));
		pan2.add(new JLabel("Id"));
		pan2.add(Box.createRigidArea(new Dimension(170,0)));
		pan2.add(new JLabel("Address"));
		pan3.add(cust_name);
		pan3.add(cust_age);
		pan3.add(cust_email);
		pan3.add(cust_id);
		pan3.add(cust_address);
		pan4.add(add_cust);
		pan4.add(edit_cust);
		pan4.add(delete_cust);
		pan4.add(find_cust);
		//pan5.add(jt1);
		pan6.add(new JLabel("Product Operations"));
		pan6.setBackground(Color.GRAY);
		pan7.add(new JLabel("Id"));
		pan7.add(Box.createRigidArea(new Dimension(150,0)));
		pan7.add(new JLabel("Name"));
		pan7.add(Box.createRigidArea(new Dimension(160,0)));
		pan7.add(new JLabel("Cantity"));
		pan7.add(Box.createRigidArea(new Dimension(150,0)));
		pan7.add(new JLabel("Price"));
		pan7.add(Box.createRigidArea(new Dimension(170,0)));
		pan7.add(new JLabel("Quality"));
		pan8.add(prod_id);
		pan8.add(prod_name);
		pan8.add(prod_cantity);
		pan8.add(prod_price);
		pan8.add(prod_quality);
		pan9.add(add_prod);
		pan9.add(edit_prod);
		pan9.add(delete_prod);
		pan9.add(find_prod);
		//pan10.add(jt2);
		pan11.add(new JLabel("Ordering"));
		pan11.setBackground(Color.white);
		pan12.add(new JLabel("OrderBy"));
		pan12.add(Box.createRigidArea(new Dimension(150,0)));
		pan12.add(new JLabel("ProductName"));
		pan12.add(Box.createRigidArea(new Dimension(160,0)));
		pan12.add(new JLabel("Address"));
		pan12.add(Box.createRigidArea(new Dimension(150,0)));
		pan12.add(new JLabel("Cantity"));
		pan13.add(order_ob);
		pan13.add(order_pn);
		pan13.add(order_a);
		pan13.add(order_c);
		pan14.add(ord);
		
		
		 this.setContentPane(p);
		    this.pack();
		    this.setTitle("Database");
		    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
	
	public String getCustID() {
        return cust_id.getText();
    }
	
	public String getCustName(){
		return cust_name.getText();
	}
	
	public String getCustAge(){
		return cust_age.getText();
	}
	
	public String getCustAddress(){
		return cust_address.getText();
	}
	
	public String getCustEmail(){
		return cust_email.getText();
	}
	
	public String getProdName(){
		return prod_name.getText();
	}
	
	public String getProdID(){
		return prod_id.getText();
	}
	
	public String getProdPrice(){
		return prod_price.getText();
	}
	
	public String getProdCantity(){
		return prod_cantity.getText();
	}
	
	public String getProdQuality(){
		return prod_quality.getText();
	}
	
	public String getOrderBy(){
		return order_ob.getText();
	}
	
	public String getProductName(){
		return order_pn.getText();
	}
	
	public String getOAddress(){
		return order_a.getText();
	}
	
	public String getOCantity(){
		return order_c.getText();
	}
	
	public void addAddDatabaseCustListener(ActionListener mal) {
        add_cust.addActionListener(mal);
    }
	
	public void addDeleteDatabaseCustListener(ActionListener mal) {
        delete_cust.addActionListener(mal);
    }
	
	public void addEditDatabaseCustListener(ActionListener mal) {
        edit_cust.addActionListener(mal);
    }
	
	public void addFindDatabaseCustListener(ActionListener mal) {
        find_cust.addActionListener(mal);
    }
	
	public void addAddDatabaseProdListener(ActionListener mal) {
        add_prod.addActionListener(mal);
    }
	
	public void addDeleteDatabaseProdListener(ActionListener mal) {
        delete_prod.addActionListener(mal);
    }
	
	public void addEditDatabaseProdListener(ActionListener mal) {
        edit_prod.addActionListener(mal);
    }
	
	public void addFindDatabaseProdListener(ActionListener mal) {
        find_prod.addActionListener(mal);
    }
    public void addOrderDatabaseListener(ActionListener mal){
    	ord.addActionListener(mal);
    }
    public void addFindCustDatabaseListener(ActionListener mal){
    	find_cust.addActionListener(mal);
    }
    public void addFindProdDatabaseListener(ActionListener mal){
    	find_prod.addActionListener(mal);
    }
}
