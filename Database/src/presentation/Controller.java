package presentation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import net.proteanit.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JTable;

import java.awt.event.*;
import java.awt.*;
import blll.OrderBLL;
import blll.CustomerBLL;
import model.Customer;
import model.Order;
import start.Mainly;
import model.Product;
import net.proteanit.sql.DbUtils;
import blll.ProductBLL;
import connection.ConFact;
import dao.COPDao;

/**
 * Prin clasa controller modelam actionlistenerii lui GUI ,prin care putem sa interactionam cu baza de date
 * @author Tam�s
 *
 */
public class Controller {
	protected static final Logger LOGGER = Logger.getLogger(Controller.class.getName());
	private View v;
	
	public Controller(View v){
		this.v=v;
		
		v.addAddDatabaseCustListener(new AddDatabaseCustListener());
		v.addAddDatabaseProdListener(new AddDatabaseProdListener());
		v.addDeleteDatabaseCustListener(new DeleteDatabaseCustListener());
		v.addDeleteDatabaseProdListener(new DeleteDatabaseProdListener());
		v.addEditDatabaseCustListener(new EditDatabaseCustListener());
		v.addEditDatabaseProdListener(new EditDatabaseProdListener());
		v.addOrderDatabaseListener(new OrderDatabaseListener());
		v.addFindCustDatabaseListener(new FindCustDatabaseListener());
		v.addFindProdDatabaseListener(new FindProdDatabaseListener());
	}
	/**
	 * Clasa pentru Butonul AddClient
	 * @author Tam�s
	 *
	 */
	
	class AddDatabaseCustListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			
			Customer c=new Customer();
			c.setID(Integer.parseInt(v.getCustID()));
			c.setName(v.getCustName());
			c.setAddress(v.getCustAddress());
			c.setAge(Integer.parseInt(v.getCustAge()));
			c.setEmail(v.getCustEmail());
			
			
			CustomerBLL custBLL=new CustomerBLL();
			
			int id = custBLL.insertCustomer(c);
			
		}
	}
	
	/**
	 * Clasa pentru butonul addProdus
	 * @author Tam�s
	 *
	 */
	
	class AddDatabaseProdListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			
			Product p=new Product();
			p.setID(Integer.parseInt(v.getProdID()));
			p.setName(v.getProdName());
			p.setPrice(Integer.parseInt(v.getProdPrice()));
			p.setCantity(Integer.parseInt(v.getProdCantity()));
			p.setQuality(Integer.parseInt(v.getProdQuality()));
			
			
			ProductBLL pBLL=new ProductBLL();
			
			int id = pBLL.insertProduct(p);
			
		}
	}
	/**
	 * Clasa pentru butonul delete client
	 * @author Tam�s
	 *
	 */
	
	class DeleteDatabaseCustListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			
			Customer c=new Customer();
			c.setID(Integer.parseInt(v.getCustID()));
			
			int idcust=c.getID();
			CustomerBLL custBLL=new CustomerBLL();
			
			int id = custBLL.deleteCustomer(idcust);
			
		}
	}
	/**
	 * Clasa pentru delete produs
	 * @author Tam�s
	 *
	 */
	
	class DeleteDatabaseProdListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			
			Product p=new Product();
			p.setID(Integer.parseInt(v.getProdID()));
			
			int idprod=p.getID();
			ProductBLL prodBLL=new ProductBLL();
			
			int id = prodBLL.deleteProduct(idprod);
			
		}
	}
	
	/**
	 * Clasa pentru butonul edit customer
	 * @author Tam�s
	 *
	 */
	class EditDatabaseCustListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			
			Customer c=new Customer();
			c.setID(Integer.parseInt(v.getCustID()));
			c.setName(v.getCustName());
			c.setAddress(v.getCustAddress());
			c.setAge(Integer.parseInt(v.getCustAge()));
			c.setEmail(v.getCustEmail());
			
			
			CustomerBLL custBLL=new CustomerBLL();
			
			int id = custBLL.updateCustomer(c);
			
		}
	}
	/**
	 * clasa pentru butonul edit produs
	 * @author Tam�s
	 *
	 */
	
	class EditDatabaseProdListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			
			Product p=new Product();
			p.setID(Integer.parseInt(v.getProdID()));
			p.setName(v.getProdName());
			p.setCantity(Integer.parseInt(v.getProdCantity()));
			p.setPrice(Integer.parseInt(v.getProdPrice()));
			p.setQuality(Integer.parseInt(v.getProdQuality()));
			
			
			ProductBLL prodBLL=new ProductBLL();
			
			int id = prodBLL.updateProduct(p);
			
		}
	}
	
	/**
	 * clasa pentru butonul order
	 * @author Tam�s
	 *
	 */
	
	class OrderDatabaseListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			
			Order o=new Order();
			o.setOrderBy(v.getOrderBy());
			o.setProductName(v.getProductName());
			o.setAddress(v.getOAddress());
			o.setCantity(Integer.parseInt(v.getOCantity()));
			Product k;
			
			
			k=COPDao.findProdByName(v.getProductName());
			//System.out.println(k.getID());
			ProductBLL prodB =new ProductBLL();
			int numb;
			numb=prodB.updateProdtoOrd(Integer.parseInt(v.getOCantity()), k);
			//System.out.println(numb);
			if (numb>0)
			{
			
			OrderBLL ordBLL=new OrderBLL();
			
		int id = ordBLL.insertOrder(o);
			
			}
			
		}
		
	}
	
	/**
	 * Clasa pentru butonul find customer
	 * @author Tam�s
	 *
	 */
	
	class FindCustDatabaseListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			JFrame frame=new JFrame("T");
			frame.setSize(600, 400);
			frame.setVisible(true);
			JTable jt1=new JTable();
			frame.add(jt1);
			
			
			
			
			Connection dbConnection = ConFact.getConnection();
			PreparedStatement findStatement = null;
			ResultSet rs = null;
			try {
				findStatement = dbConnection.prepareStatement(COPDao.jTString1);
				rs = findStatement.executeQuery();
				jt1.setModel(DbUtils.resultSetToTableModel(rs));
				
			
				
				

			} catch (SQLException ett) {
				LOGGER.log(Level.WARNING,"COPDao:populate " + ett.getMessage());
			} finally {
				ConFact.close(rs);
				ConFact.close(findStatement);
				ConFact.close(dbConnection);
			}
				
			
		}
	}
	/**
	 * Clasa pentru butonul finproduct
	 * @author Tam�s
	 *
	 */
	
	
	class FindProdDatabaseListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			JFrame frame=new JFrame("T");
			frame.setSize(600, 400);
			frame.setVisible(true);
			JTable jt1=new JTable();
			frame.add(jt1);
			
			
			
			
			Connection dbConnection = ConFact.getConnection();
			PreparedStatement findStatement = null;
			ResultSet rs = null;
			try {
				findStatement = dbConnection.prepareStatement(COPDao.jTString2);
				rs = findStatement.executeQuery();
				jt1.setModel(DbUtils.resultSetToTableModel(rs));
				
			
				
				

			} catch (SQLException ett) {
				LOGGER.log(Level.WARNING,"COPDao:populate " + ett.getMessage());
			} finally {
				ConFact.close(rs);
				ConFact.close(findStatement);
				ConFact.close(dbConnection);
			}
				
			
		}
	}
	
	
	

}
