package start;



import blll.CustomerBLL;

import model.Customer;
import presentation.Controller;

import presentation.View;
/**
 * Clasa main creaza un View si un controller
 * @author Tam�s
 *
 */

public class Mainly {
	
	
	public static void main(String[] args){
		
		View v=new View();
		v.setVisible(true);
		Controller c=new Controller(v);
		
	}

}
