package bll.validators;

import model.Customer;
/**Clasa valideaza un Customer pe care vrem sa-l adaugam in baza de date
*/
public class CustomerAgeValidator implements Validator<Customer> {
	private static final int MIN_AGE = 7;
	private static final int MAX_AGE = 90;

	
	/**
	 * Metoda decide daca Custumer introdus are varsta intra doua limite deja specificate
	 * @param t Customer pe care vrem sa-l introducem in baza de date
	 */
	public void validate(Customer t) {

		if (t.getAge() < MIN_AGE || t.getAge() > MAX_AGE) {
			throw new IllegalArgumentException("The Customer Age limit is not respected!");
		}

	}

}