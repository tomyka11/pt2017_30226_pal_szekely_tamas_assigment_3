package bll.validators;
/**
 * 
 * @author Tam�s
 *Interfata pentru Clasele Validate
 * @param <T>
 */

public interface Validator<T> {

	public void validate(T t);
}